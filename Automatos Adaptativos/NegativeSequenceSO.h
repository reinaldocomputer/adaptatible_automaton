#pragma once
#include "AAFO.h"
/***
Sequ�ncia negativa de segunda ordem s�o os pares adaptativos de primeira ordem que devem ser acrescentados ao aut�mato. Neste framework a
sequ�ncia engativa de segunda ordem � implementada como classe negativeSequenceSO.

***/
class negativeSequenceSO
{
public:
	atribPairFO foreignTransition;
	negativeSequenceSO(atribPairFO foreignTransition);
	negativeSequenceSO();
	
};

