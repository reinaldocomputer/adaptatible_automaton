#include "automaton.h"



automaton::automaton(string _name, atribAlphabet _Alphabet, atribState _states,
	atribState _initialStates, atribState _finalStates, atribTransition _transitions)
{
	this->_name = _name;
	this->_Alphabet = _Alphabet;
	this->_states = _states;
	this->_initialStates = _initialStates;
	this->_finalStates = _finalStates;
	this->_transitions = _transitions;
}
	

/**/
string automaton::getName() {
	return _name;
}

void automaton::setName(string _name) {
	this->_name = _name;
}

atribAlphabet automaton::getAlphabet() {
	return this->_Alphabet;
}

void automaton::setAlphabet(atribAlphabet _Alphabet) {
	this->_Alphabet = _Alphabet;
}

atribState automaton::getStates() {
	return this->_states;
}

void automaton::setStates(atribState _states) {
	this->_states = _states;
}

atribState automaton::getInitialStates() {
	return _initialStates;
}

void automaton::setInitialStates(atribState _initialStates) {
	this->_initialStates = _initialStates;
}


atribState automaton::getFinalStates() {
	return _finalStates;
}

void automaton::setFinalStates(atribState _finalStates) {
	this->_finalStates = _finalStates;
}

atribTransition automaton::getTransitions() {
	return _transitions;
}

void automaton::setTransitions(atribTransition _transitions) {
	this->_transitions = _transitions;
}

void automaton::printStates()
{
	cout << endl;
	cout << _name << endl << "States: " << endl;
	atribState::iterator it = _states.begin();

	for (it; it != _states.end(); it++)
	{
		cout << "ID: " << it->second << " " << "Nome: " << it->first._name << endl;
	}
}


void automaton::printInitialStates()
{
	cout << endl;
	cout << _name << endl << "States Intials: " << endl;
	atribState::iterator it = _initialStates.begin();

	for (it; it != _initialStates.end(); it++)
	{
		cout << "ID: " << it->second << " " << "Nome: " << it->first._name << endl;
	}

}

void automaton::printFinalStates()
{
	cout << endl;
	cout << _name << endl << "States Finals: " << endl;
	atribState::iterator it = _finalStates.begin();

	for (it; it != _finalStates.end(); it++)
	{
		cout << "ID: " << it->second << " " << "Nome: " << it->first._name << endl;
	}

}

void automaton::printTransitions()
{
	cout << "=Begin=" << endl;
	cout << "=Transition=" << endl;
	atribTransition::iterator it = _transitions.begin();

	for (it; it != _transitions.end(); it++)
	{
		cout << "ID: " << it->first << endl;
		transition current = it->second;
		cout << "Origin: " << current.getOrigin()._name << endl;
		cout << "Symbol: " << current.getSymbol() << endl;
		cout << "Destiny: " << current.getDestiny()._name << endl;
		cout << endl;
	}
	cout << "=End=" << endl;
}

void automaton::printAlphabet() {
	atribAlphabet::iterator it = _Alphabet.begin();
	for (it; it != _Alphabet.end(); it++) {
		cout << it->first << " " << it->second << endl;
	}
}

atribTransition automaton::Sch(state stateOrigin, string symbol, state from) {
	atribTransition output;

	map<int, transition> ::iterator it = _transitions.begin();

	for (it; it != _transitions.end(); it++) {
		string ori = it->second.getOrigin()._name, sim = it->second.getSymbol(),
			des = it->second.getDestiny()._name;
		if (ori == stateOrigin._name && sim == symbol && des == from._name) {
			pair <int, transition> _new(it->first, it->second);
			output.insert(_new);
		}
	}
	return output;
}

atribTransition automaton::Sch(state stateOrigin, state destiny) {
	atribTransition output;

	atribTransition::iterator it = _transitions.begin();

	for (it; it != _transitions.end(); it++) {
		string ori = it->second.getOrigin()._name, sim = it->second.getSymbol(), des = it->second.getDestiny()._name;
		if (stateOrigin == ori && destiny == des) {
			pair <int, transition> _new(it->first, it->second);
			output.insert(_new);
		}
	}
	return output;
}

atribTransition automaton::Sch(state State, bool stateOrigin) {
	atribTransition output;

	atribTransition::iterator it = _transitions.begin();

	for (it; it != _transitions.end(); it++) {
		string ori = it->second.getOrigin()._name, sim = it->second.getSymbol(), des = it->second.getDestiny()._name;
		if (stateOrigin) {
			if (State == ori) {
				pair <int, transition> _new(it->first, it->second);
				output.insert(_new);
			}
		}
		else {
			if (State == des) {
				pair <int, transition> _new(it->first, it->second);
				output.insert(_new);
			}
		}
	}
	return output;
}

atribTransition automaton::Sch(state State, string symbol, bool stateOrigin) {
	atribTransition output;

	atribTransition::iterator it = _transitions.begin();

	for (it; it != _transitions.end(); it++) {
		string ori = it->second.getOrigin()._name, sim = it->second.getSymbol(), des = it->second.getDestiny()._name;
		if (stateOrigin) {
			if (State == ori && symbol == sim) {
				pair <int, transition> _new(it->first, it->second);
				output.insert(_new);
			}
		}
		else {
			if (State == des && symbol == sim) {
				pair <int, transition> _new(it->first, it->second);
				output.insert(_new);
			}
		}
	}
	return output;
}

atribTransition automaton::Sch(string symbol) {
	atribTransition output;

	atribTransition::iterator it = _transitions.begin();

	for (it; it != _transitions.end(); it++) {
		string ori = it->second.getOrigin()._name, sim = it->second.getSymbol(), des = it->second.getDestiny()._name;
		if (symbol == sim) {
			pair <int, transition> _new(it->first, it->second);
			output.insert(_new);
		}
	}
	return output;
}

void automaton::rem(string Symbol) {
	atribAlphabet::iterator it = _Alphabet.find(Symbol);
	if (it != _Alphabet.end()) {
		_Alphabet.erase(it);
		cout << mRemSymbol << endl;
	}

}

void automaton::ins(string Symbol) {
	pair <string, int> symbol(Symbol, _Alphabet.size());
	_Alphabet.insert(symbol);
	cout << mInsSymbol << endl;
}

void automaton::rem(state State) {
	atribState::iterator it = _states.find(State);
	if (it != _states.end()) {
		_states.erase(it);
		cout << mRemState << endl;
	}

}

void automaton::ins(state _State) {
	pair <state, int> _new(_State, _states.size());
	_states.insert(_new);
	cout << mInsState << endl;
}

void automaton::ins(transition _transition) {	
	pair <int, transition> _new(this->_transitions.size(), _transition);
	this->_transitions.insert(_new);
	cout << mInsTransition << endl;
}

void automaton::ins(pair <int, transition> _transition) {
	pair <int, transition> _new = _transition;
	this->_transitions.insert(_new);
	cout << mInsTransition << endl;
}

void automaton::addInitialState(state initialState)
{
	this->_initialStates.insert(make_pair(initialState, this->_states.find(initialState)->second));
}

void automaton::addFinalState(state finalState)
{
	this->_finalStates.insert(make_pair(finalState, this->_states.find(finalState)->second));
}

void automaton::rem(transition _transition) {
	 atribTransition::iterator it = this->_transitions.begin();
	for (it; it != this->_transitions.end(); it++) {
		if (it->second.getOrigin() == _transition.getOrigin() &&
			it->second.getDestiny() == _transition.getDestiny() &&
			it->second.getSymbol() == _transition.getSymbol()){ 
			_transitions.erase(it);
			cout << mRemTransition << endl;
		}			
	}

}



