#include "myitoa.h"

std::string myitoa::operation(int number)
{
	std::string operation = "";
	while (number > 0)
	{
		char aux = ((char)(number % 10));
		operation += aux + '0';
		number /= 10;
	}

	return operation;
}
