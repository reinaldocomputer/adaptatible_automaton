#include "DFA.h"

bool DFA::output(string word, state current) {
	bool out = false;

	atribTransition mapTran = Sch(current, 1);
	atribTransition::iterator itEmpty = mapTran.begin();
	for (itEmpty; itEmpty != mapTran.end(); itEmpty++) {
		itEmpty->second.print();
		if (itEmpty->second.getSymbol() == symEmpty) {
			out |= output(word, itEmpty->second.getDestiny());
		}
	}

	if (word.empty()) {
		atribState::iterator it = _finalStates.begin();
		for (it; it != _finalStates.end(); it++) {
			if (current == it->first) return true;
		}
		cout << endl;

	}
	else {
		string auxString = word.substr(0, 1);
		word.erase(0, 1);



		atribTransition mapTran = Sch(current, auxString, 1);
		atribTransition::iterator it = mapTran.begin();
		for (it; it != mapTran.end(); it++) {
			if (it->second.getSymbol() == auxString)
				out |= output(word, it->second.getDestiny());
		}
	}



	return out;
}

bool DFA::output(string word)
{
	return output(word, this->getInitialStates().begin()->first);
}
