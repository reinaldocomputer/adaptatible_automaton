#include <iostream>
#include "AASO.h"

using namespace std;

int main() {
	NFA l1;

	l1.ins(state("q1"));
	l1.ins(state("q2"));
	l1.ins(state("q3"));

	l1.ins("a");
	l1.ins("b");
	
	l1.ins(transition(state("q1"),"a", state("q2")));
	l1.ins(transition(state("q2"), "b", state("q3")));


	l1.addInitialState(state("q1"));
	l1.addFinalState(state("q3"));

	string entrada;

	while (cin >> entrada) {

		if (l1.output(entrada)) {
			cout << "Aceita" << endl;
		}
		else {
			cout << "Nao aceita" << endl;
		}
	}


	return 0;
}