#pragma once
#include "AAFO.h"
/***
Sequ�ncia positiva de segunda ordem s�o os pares adaptativos de primeira ordem que devem ser removidos do aut�mato. Aqui representada pela
classe positiveSequenceSO.

***/
class positiveSequenceSO
{
public:
	atribPairFO ownTransition;
	positiveSequenceSO(atribPairFO ownTransition);
	positiveSequenceSO();
	
};

