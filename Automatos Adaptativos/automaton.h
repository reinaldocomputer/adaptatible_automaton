#pragma once
#include "transition.h"
#include <map>
#include <vector>
#include <set>
#include <queue>
#include "strings.h"

typedef map <string, int> atribAlphabet;
typedef map <state, int> atribState;
typedef map <int, transition> atribTransition;
typedef pair <state, int> atribStateInitial;
#define fore(i,n) for(int i = 0; i < n; i++)
#define symEmpty "-1"
/***
Existem diferentes tipos de autômatos que devem ser escolhidos de maneira apropriada para cada situação. Nesse framework traz implementado a estrutura dos
autômatos finitos determínisticos, finítos não determinísticos, adaptativos de primeira e segunda ordem.
Cada autômato aqui implementado tem em sua base a classe automaton que possui os elementos básicos dos autômatos, sendo eles o alfabeto, estados finais,
estados iniciais, transições e o nome do autômato.
Na classe automaton estão implmementados também os métodos padrões da programação objeto como getters e setters, métodos do modelo teórico, e métodos para visualização.
Os métodos do modelo teórico são os Sch, rem e ins que possuem a função de busca, remoção e inserção extremamente úteis para a adaptatividade.
E para a visualização os métodos printStates, printInitialStates, printFinalStates, printTransitions, printAlphabet que possuem a função de 
imprimir todos os estados do autômato, imprimir os estados definidos como iniciais, imprimir os estados definidos como finais, imprimir as 
transições pertencentes ao autômato e imprimir os símbolos que o autômato reconhece.

*/
class automaton
{
public:
	//Attributes
	string _name;
	atribAlphabet _Alphabet;
	atribState _states, _finalStates;
	atribState _initialStates;
	atribTransition _transitions;

	//Methods
	automaton(string _name, atribAlphabet _Alphabet, atribState _states, atribState _initialStates, atribState _finalStates, atribTransition _transitions);
	automaton() {}
	automaton::string getName();
	void setName(string _name);
	atribAlphabet getAlphabet();
	void setAlphabet(atribAlphabet _Alphabet);
	atribState getStates();
	void setStates(atribState _states);
	atribState getInitialStates();
	void setInitialStates(atribState _initialStates);
	atribState getFinalStates();
	void setFinalStates(atribState _finalStates);
	atribTransition getTransitions();
	void setTransitions(atribTransition _transitions);
	void printStates();
	void printInitialStates();
	void printFinalStates();
	void printTransitions();
	void printAlphabet();
	atribTransition Sch(state stateOrigin, string symbol, state from);
	atribTransition Sch(state stateOrigin, state chegada);
	atribTransition Sch(state State, bool stateOrigin);
	atribTransition Sch(state State, string symbol, bool stateOrigin);
	atribTransition Sch(string symbol);
	void rem(state State);
	void rem(string Symbol);
	void rem(transition transition);
	void ins(string Symbol);
	void ins(state State);
	void ins(transition transition);
	void ins(pair <int, transition> transition);
	void addInitialState(state initialState);
	void addFinalState(state finalState);

};

