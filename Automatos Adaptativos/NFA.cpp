#include "NFA.h"


bool NFA::output(string word) {
	atribState::iterator it = this->_initialStates.begin();
	return output(word, it->first);
}

bool NFA::output(string word, state current) {
	bool out = false;

	atribTransition mapTran = this->Sch(current, 1);
	atribTransition::iterator itVazio = mapTran.begin();
	for (itVazio; itVazio != mapTran.end(); itVazio++) {
		itVazio->second.print();
		if (itVazio->second.getSymbol() == symEmpty) {
			out |= output(word, itVazio->second.getDestiny());
		}
	}

	if (word.empty()) {
		atribState::iterator it = this->_finalStates.begin();
		for (it; it != this->_finalStates.end(); it++) {
			if (current == it->first) return true;
		}
		cout << endl;

	}
	else {
		string a = word.substr(0, 1);
		word.erase(0, 1);



		atribTransition mapTran = Sch(current, a, 1);
		atribTransition::iterator it = mapTran.begin();
		for (it; it != mapTran.end(); it++) {
			if (it->second.getSymbol() == a)
				out |= output(word, it->second.getDestiny());
		}
	}



	return out;
}

void NFA :: removeSymbVazio(atribTransition &_transitions) {

	vector <bool> visited(this->_states.size());
	vector < pair <state, int> > _line;
	atribTransition emptysmap = Sch(symEmpty);
	atribState::iterator it = this->_states.begin();
	int auxCount = this->_transitions.size();


	for (it; it != this->_states.end(); it++) {
		pair <state, int> linesunit(it->first, it->second);
		_line.push_back(linesunit);

		while (!_line.empty()) {
			atribTransition tempTrans;
			pair <state, int> a = _line.front();
			_line.erase(_line.begin());

			if (!visited[a.second]) {
				visited[a.second] = true;

				atribTransition _test = Sch(a.first, 1);
				atribTransition::iterator itTrans = _test.begin();
				for (itTrans; itTrans != _test.end(); itTrans++) {
					pair <int, transition> _new(itTrans->first, itTrans->second);
					tempTrans.insert(_new);
				}

				while (!tempTrans.empty()) {
					itTrans = tempTrans.begin();
					pair <int, transition> currentTran(itTrans->first, itTrans->second);
					tempTrans.erase(itTrans);
			
					if (currentTran.second.getSymbol() != symEmpty) {
						itTrans = emptysmap.begin();
						for (itTrans; itTrans != emptysmap.end(); itTrans++) {
							if (itTrans->second.getOrigin() == currentTran.second.getDestiny()) {
								transition n1(currentTran.second.getOrigin(), currentTran.second.getSymbol(), itTrans->second.getDestiny());
								pair <int, transition> n2(auxCount++, n1);
								tempTrans.insert(n2);
							}
						}

						_transitions.insert(currentTran);
					}
				}
			}
		}
	}
}

void NFA::toAFNDG() {
	int initialPosition = this->_states.size(); state a("Initial");
	pair <state, int> initialState(a, initialPosition);
	this->_states.insert(initialState);


	int positionFinal = this->_states.size(); state b("Final");
	pair <state, int> estadoFinal(b, positionFinal);
	this->_states.insert(estadoFinal);

	while (!this->_initialStates.empty()) {
		atribState::iterator it = this->_initialStates.begin();
		pair <state, int> current(it->first, it->second);
		this->_initialStates.erase(it);
		transition a1(a, symEmpty, current.first);
		int positiontransition = this->_transitions.size();
		pair <int, transition> b1(positiontransition, a1);
		this->_transitions.insert(b1);
	}

	this->_initialStates.insert(initialState);
	while (!this->_finalStates.empty()) {
		atribState::iterator it = this->_finalStates.begin();
		pair <state, int> current(it->first, it->second);
		this->_finalStates.erase(it);
		transition a1(current.first._name, symEmpty, b);
		int positiontransition = this->_transitions.size();
		pair <int, transition> b1(positiontransition, a1);
		this->_transitions.insert(b1);
	}

	this->_finalStates.insert(estadoFinal);


}

atribState NFA::makefinalstates(atribState &statesDFA) {
	atribState DFAsfinal;
	atribState::iterator it = statesDFA.begin();

	for (it; it != statesDFA.end(); it++) {
		atribState::iterator it2 = this->_finalStates.begin();
		for (it2; it2 != this->_finalStates.end(); it2++) {
			if (it->first._name.find(it2->first._name) != -1) {
				DFAsfinal.insert(*it);
			}
		}
	}

	return DFAsfinal;

}

atribState NFA::toDeterministic(atribTransition &transitionsTemp) {
	atribState statesDFA;
	atribTransition transitionsDFA;

	int countE = 0, countT = 0;
	atribState::iterator itEst = this->_states.begin();

	queue <state> _line;
	_line.push(_initialStates.begin()->first);
	
	while (!_line.empty()) {
		state current = _line.front();
		_line.pop();		

		atribState::iterator itverify = statesDFA.find(current);

		if (itverify == statesDFA.end()) {
			
			atribAlphabet::iterator itAlf = this->_Alphabet.begin();

			for (itAlf; itAlf != this->_Alphabet.end(); itAlf++) {
		

				if (itAlf->first != symEmpty) {

					atribTransition mapTran;
					atribTransition::iterator itTrans = transitionsTemp.begin();
					
					for (itTrans; itTrans != transitionsTemp.end(); itTrans++) { //OK						
						if (itTrans->second.getOrigin() == current && itTrans->second.getSymbol() == itAlf->first) mapTran.insert(*itTrans);
					}

					itTrans = mapTran.begin();

					if (mapTran.size() > 1) {						
						set <string> _name;
						vector < pair <state, string> > destinies;

						for (itTrans; itTrans != mapTran.end(); itTrans++) {					
							_name.insert(itTrans->second.getDestiny()._name);
							atribTransition::iterator tranDest = transitionsTemp.begin();
							for (tranDest; tranDest != transitionsTemp.end(); tranDest++) {

								if (tranDest->second.getOrigin() == itTrans->second.getDestiny()) {
									pair <state, string> _new(tranDest->second.getDestiny(), tranDest->second.getSymbol());									
									destinies.push_back(_new);
								}
							}
						}
						
						set <string> ::iterator itSet = _name.begin();
						string _nameEst = "";
						for (itSet; itSet != _name.end(); itSet++) {
							_nameEst += *itSet;
						}

						state _newstate(_nameEst);
						atribState::iterator itTeste = statesDFA.find(current);
						if (itTeste == statesDFA.end()) {
							pair <state, int> pstate(current, countE++);
							statesDFA.insert(pstate);
						}
						
						transition currenttransition(current, itAlf->first, _newstate);
						pair <int, transition> currentP(countT++, currenttransition);
						transitionsDFA.insert(currentP);

						_line.push(_newstate); 

						fore(iDes, destinies.size()) {

							transition _newT(_newstate, destinies[iDes].second, destinies[iDes].first);
							pair <int, transition> ptransition(transitionsTemp.size() + 1, _newT);
							transitionsTemp.insert(ptransition);													

						}

					}
					else if (mapTran.size() == 1) {													

						pair <int, transition> p1(countT++, itTrans->second);
						transitionsDFA.insert(p1); 

						atribState::iterator itTeste = statesDFA.find(current);
						if (itTeste == statesDFA.end()) {
							pair <state, int> pstates(current, countE++);
							statesDFA.insert(pstates);
						}
						_line.push(itTrans->second.getDestiny());

					}
					else {						
						pair <state, int> pstates(current, countE++);
						statesDFA.insert(pstates);
					}


				}
			}
		}
	}

	transitionsTemp = transitionsDFA;

	return statesDFA;

}

DFA NFA :: toDFA() {

	atribTransition DFATransitions;
	this->removeSymbVazio(DFATransitions);
	atribState DFAstates = toDeterministic(DFATransitions);
	atribAlphabet DFAsalphabet = _Alphabet; _Alphabet.erase(_Alphabet.find(symEmpty));
	atribState initialStateDFA = _initialStates;
	atribState _finalStatesDFA = this->makefinalstates(DFAstates);
	DFA output(this->_name + " DFA", DFAsalphabet, DFAstates, initialStateDFA, _finalStatesDFA, DFATransitions);

	return output;
}
