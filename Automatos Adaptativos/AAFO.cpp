#include "AAFO.h"
#include <sstream>
AAFO::AAFO(string _name, atribAlphabet alphabet, atribState _state, atribState _initialStates, atribState _finalStates, atribTransition _transitions) : NFA(_name, alphabet, _state, _initialStates, _finalStates, _transitions)
{	
	cout << mCreateAutomataFO1  << endl;
}

AAFO::AAFO(string _name, atribAlphabet alphabet, atribState _state, atribState _initialStates, atribState _finalStates, atribTransition _transitions, atribPairFO setoftests) : NFA(_name, alphabet, _state, _initialStates, _finalStates, _transitions)
{
	this->setoftests = setoftests;
	cout << mCreateAutomataFO2 << endl;
}

AAFO::AAFO()
{
}

bool AAFO::output(string word, state current, AAFO currentAutomaton) {
	bool flag = true;
	bool out = false;
	AAFO nextAutomaton = currentAutomaton;
		
	atribTransition mapTran = Sch(current, 1);
	atribTransition::iterator it = mapTran.begin();

	if (word.empty()) {
		atribState::iterator it = _finalStates.begin();
		for (it; it != _finalStates.end(); it++) {
			if (current == it->first) return true;
		}

	}
	else {	
		string a = word.substr(0, 1);
		word.erase(0, 1);
		flag = false;
		atribTransition mapTran = currentAutomaton.Sch(current, a, 1);
		atribTransition::iterator it = mapTran.begin();
		for (it; it != mapTran.end(); it++) {
			pair <int, transition> tranCurrent = *it;

			if (tranCurrent.second.getSymbol() == a) {			
				atribPairFO::iterator currentPAIR = this->setoftests.find(tranCurrent.first);
					
				if (currentPAIR != setoftests.end()) {		
					atribTransition::iterator itTrans;
					negativeSequence currentNegative = currentPAIR->second._for;
					positiveSequence currentPositive = currentPAIR->second._own;
					vector < pair<int, transition> > aux;					
					itTrans = currentNegative.foreignTransition.begin();
					int _pairux= 1;
					for (itTrans; itTrans != currentNegative.foreignTransition.end(); itTrans++) {
						transition currenttransition = itTrans->second;
						state origin = currenttransition.Start();
						if (origin.activated) { 
											  
							if (origin._name == dynamicState) {
								string q = "s";
								char aux[10];
								int result = (currentAutomaton._states.size() + 1);
								myitoa opItoa;
								q += opItoa.operation(result); 
								origin._name = q;
								
							}
							else {
								atribTransition itTrans2 = currentAutomaton.Sch(origin, origin.symbol, origin.stateOrigin);
								origin = itTrans2.begin()->second.getOrigin();
							}
						}
						
						state destiny = currenttransition.getDestiny();


						if (destiny == dynamicState) {
							string q = "s";
							char aux[10];
							q += myitoa().operation(currentAutomaton._states.size() + 1);
							destiny = state(q);							
							nextAutomaton.ins(destiny); 
						}

						transition _newTran(origin, currenttransition.getSymbol(), destiny);
						aux.push_back(make_pair(counter++ + 100, _newTran));


					}

					itTrans = currentPositive.ownTransition.begin();
					for (itTrans; itTrans != currentPositive.ownTransition.end(); itTrans++) {
						transition transCurrent = itTrans->second;
						state origin = transCurrent.Start();
						transition ownTransition(origin, transCurrent.getSymbol(), transCurrent.Fin());
						if (origin.activated) { 
							atribTransition mapAux;
							mapAux = currentAutomaton.Sch(origin, origin.symbol, origin.stateOrigin);						
							origin = mapAux.begin()->second.getOrigin();
							ownTransition = transition(origin, transCurrent.getSymbol(), transCurrent.Fin());
						}

						nextAutomaton.rem(ownTransition);

					}

					fore(i, aux.size()) {						
						nextAutomaton.ins(aux[i]);
					}
				}

				out |= output(word, it->second.getDestiny(), nextAutomaton);
			}
		}
	}

	for (it; it != mapTran.end() && flag; it++) {
		if (it->second.getSymbol() == symEmpty) {
			atribPairFO::iterator currentPAIR = setoftests.find(it->first);
			if (currentPAIR != setoftests.end()) {
				atribTransition::iterator itTrans;
				negativeSequence currentNegative = currentPAIR->second._for;
				positiveSequence currentPositive = currentPAIR->second._own;
				vector < pair<int, transition> > aux;
				itTrans = currentNegative.foreignTransition.begin();

				for (itTrans; itTrans != currentNegative.foreignTransition.end(); itTrans++) {				
					transition currenttransition = itTrans->second;
					state origin = currenttransition.Start();					
					if (origin.activated) { 

						if (origin._name == dynamicState) {
							string q = "s";
							char aux[10];
							q += "" + currentAutomaton._states.size() + 1;
							origin = state(q);;
						}
						else {
							atribTransition itTrans2 = currentAutomaton.Sch(origin, origin.symbol, origin.stateOrigin);
							origin = itTrans2.begin()->second.getOrigin(); //Currentiza origin
						}
					}

					state destiny = currenttransition.getDestiny();


					if (destiny == dynamicState) {
						string q = "s";
						char aux[10];
						q += "" + currentAutomaton._states.size() + 1;
						destiny = state(q);
						nextAutomaton.ins(destiny);
					}

					transition _newTran(origin, currenttransition.getSymbol(), destiny);					
					aux.push_back(make_pair(counter++ + 100, _newTran));


				}
				
				itTrans = currentPositive.ownTransition.begin();
				for (itTrans; itTrans != currentPositive.ownTransition.end(); itTrans++) {
					transition transCurrent = itTrans->second;
					state origin = transCurrent.Start();
					transition ownTransition(origin, transCurrent.getSymbol(), transCurrent.Fin()); 
					if (origin.activated) {
						atribTransition mapAux = currentAutomaton.Sch(origin, origin.symbol, origin.stateOrigin);
						origin = mapAux.begin()->second.getOrigin();
						ownTransition = transition(origin, transCurrent.getSymbol(), transCurrent.Fin());
					}

					nextAutomaton.rem(ownTransition);

				}

				fore(i, aux.size()) {					
					nextAutomaton.ins(aux[i]);
				}
			}
			out |= output(word, it->second.getDestiny(), nextAutomaton);
			if (out) return true;
		}
	}





	return out;
}

bool AAFO::output(string word)
{
	return output(word, this->getInitialStates().begin()->first,*this);
}

void AAFO::insFO(pairFO _pair) {
	pair <int, pairFO> _new(setoftests.size(), _pair);
	this->setoftests.insert(_new);
	cout << mInsFO << endl;
}

void AAFO::insFO(pair <int, pairFO> _pair) {
	pair <int, pairFO> _new = _pair;
	this->setoftests.insert(_new);
	cout << mInsFO << endl;
}

void AAFO::remFO(int id) {
	atribPairFO::iterator it = setoftests.find(id);
	if (it != setoftests.end()) {
		setoftests.erase(it);		
		cout << mRemFO << endl;
	}
	else {
		cout << mErrorRemSO << id << endl;
	}
}
atribPairFO AAFO::getSetOfTests() {
	return setoftests;
}

void AAFO::setSetOfTests(atribPairFO setoftests) {
	this->setoftests = setoftests;
}

void AAFO::printSetOfTests() {
	atribPairFO::iterator it = setoftests.begin();
	cout << "==Begin=" << endl;
	cout << "=Set of Tests=" << endl;
	for (it; it != setoftests.end(); it++) {
		cout << "ID: " << it->first << endl;
		atribTransition::iterator itTrans = it->second._own.ownTransition.begin();
		for (itTrans; itTrans != it->second._own.ownTransition.end(); itTrans++) {
			transition current = itTrans->second;
			cout << current.shape << endl;
		}		
		itTrans = it->second._for.foreignTransition.begin();
		for (itTrans; itTrans != it->second._for.foreignTransition.end(); itTrans++) {
			transition current = itTrans->second;
			cout << current.shape << endl;
		}		
	}
	cout << "==End=" << endl;
}