#pragma once
#include "NFA.h"
#include "pairFO.h"
#include "myitoa.h"
#define dynamicState "qs"
static int counter = 1;
/***
Um automato adaptativo de primeira ordem tem como seu dispositivo subjacente um aut�mato finito n�o-determin�stico nesse modelo. A classe
AAFO abrevia��o para Adaptatible Automata First Order (Automato Adaptativo de Primeira Ordem) � capaz de lidar com aut�matos adaptativos que
podem ser reconhecedores de linguagens livres de contexto.
A classe tem um novo atributo chamado setoftests que � o conjunto de comportamentos do automato de primeira ordem. Os elementos do conjunto de 
comportamentos � um par que possui um ID e um pairFO (pair First Order) que � um par com uma sequ�ncia positiva e negativa do que deve ser
acrescentado ou removido do dispositivo subjacente.
As novos m�todos implementados na classe s�o insFO, remFO e os getters e setters relativos ao setoftests. Os m�todos insFO e remFO s�o para remover
pares adaptativos de primeira ordem do conjunto de comportamento (setoftests).
Uma importante observa��o � que o ID do par adaptativo faz refer�ncia a qual transi��o do dispositivo subjacente ele est� relacionado, ou seja,
qual ser� a transi��o que o ativar�. Logo o ID do par adaptativo e da transi��o necessitam ser correspondentes.
***/
typedef map <int, pairFO> atribPairFO; 

class AAFO :
	public NFA
{
public:
	atribPairFO setoftests;

	AAFO(string _name, atribAlphabet alphabet, atribState _state, atribState _initialStates, atribState _finalStates, atribTransition _transitions);
	AAFO(string _name, atribAlphabet alphabet, atribState _state, atribState _initialStates, atribState _finalStates, atribTransition _transitions, atribPairFO setoftests);
	AAFO();

	bool output(string word, state current, AAFO currentAutomaton);
	bool output(string word);

	void insFO(pairFO _pair);

	void insFO(pair<int, pairFO> _pair);

	void remFO(int id);

	atribPairFO getSetOfTests();

	void setSetOfTests(atribPairFO setoftests);

	void printSetOfTests();

};

