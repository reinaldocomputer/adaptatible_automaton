#pragma once
#include "automaton.h"
/***
Sequ�ncias positivas s�o transi��es pr�prias que devem ser removidas do aut�mato aqui representada pela classe positiveSequence.
***/
class positiveSequence
{
public:
	atribTransition ownTransition;
	positiveSequence(atribTransition ownTransition);
	positiveSequence() {}

};

