#include "state.h"
 
state::state(string _name) {
	if (_name == StateDynamic)
		this->activated = true;
	else
		this->activated = false;

	this->stateOrigin = false;
	this->_name = _name;
	this->symbol = "";

}
state::state() {
	this->_name = "";
}
state::state(string _name, string symbol, bool stateOrigin) {
	this->activated = true;
	this->_name = _name;
	this->symbol = symbol;
	this->stateOrigin = stateOrigin;
}

