#pragma once
#include "AAFO.h"
#include "pairSO.h"
typedef map <int, pairSO> atribPairSO;
/***
Um aut�mato adaptativo de segunda ordem tem como dispositivo subjacente um aut�mato adaptativo de primeira ordem, ou seja, um aut�mato adaptativo
de segunda ordem adiciona e remove pares adaptativos de primeira ordem. Uma vez que um aut�mato adaptativo de primeira ordem pode ser um reconhecedor
para uma linguagem livre de contexto ao modificar o automato de primeira ordem a linguagem o reconhecedor � alterado podendo reconhecer uma nova linguagem.
A classe AASO � a camada respons�vel pela adaptatividade de segunda ordem com capacidade de inferir um aut�mato de primeira ordem aprendendo assim uma
linguagem.
AASO tem tr�s novos atributos chamados de setoftestsSO, inferedAutomata e flag. O atributo setoftestsSO semelhante ao setoftests da classe AAFO � o conjunto de
comportamentos do AASO sendo seus elementos pares de inteiros e pares adaptativos de segunda ordem, isto �, o inteiro � o ID dando refer�ncia a transi��o
que ativa as adapta��es que devem ser feitas e os pares adaptativos de segunda ordem s�o os pairSO que s�o sequencias positivas e negativas de pares adaptativos
de primeira ordem que devem ser inclu�dos e removidos do AASO. Al�m disso o atributo inferedAutomata � atualizado ap�s um automato ser inferido pelo m�todo inferAutomata,
sendo assim capaz de agir como um reconhecedor para a linguagem inferida.
Os novos m�todos implementados na classe s�o o inferAutomata, insSO e remSO. O m�todo inferAutomata cada vez que � utilizado transforma��es s�o
feitas no dispositivo subjacente (inferedAutomata) que ap�s se estabilizar atualiza o atributo flag para o valor true significando que ouve uma
converg�ncia e foi aprendida uma linguagem.
***/
class AASO :
	public AAFO
{
private:
	void update();
	
protected:
	bool inferAutomata(string word, state current, AASO currentAutomata);
public:
	atribPairSO setoftestsSO;
	AAFO inferedAutomata;

	bool flag;
	AASO(string _name, atribAlphabet alphabet, atribState _state, atribState _initialStates, atribState _finalStates, atribTransition _transitions);
	AASO(string _name, atribAlphabet alphabet, atribState _state, atribState _initialStates, atribState _finalStates, atribTransition _transitions, atribPairFO setoftests);
	AASO(string _name, atribAlphabet alphabet, atribState _state, atribState _initialStates, atribState _finalStates, atribTransition _transitions, atribPairFO setoftests, atribPairSO setoftestsSO);
	AASO(){}
	void insSO(pair <int, pairSO> _new);
	void remSO(int id);	
	bool inferAutomata(string word);
	

};


