#pragma once
#include "positiveSequenceSO.h"
#include "NegativeSequenceSO.h"
/***
Um par adaptativo de segunda ordem � um par que possui uma sequ�ncia positiva e uma sequ�ncia negativa que s�o os pares adaptativos de primeira
ordem que devem ser removidos e acrescentados ao aut�mato de primeira ordem. 
A classe pairSO implementa o par ordenado de segunda ordem que possuem pares ordenados de primeira ordem que devem ser removidos ou acrescentados
ao aut�mato modificando assim o mesmo.

***/
class pairSO
{
public:
	positiveSequenceSO _own;
	negativeSequenceSO _for;
	pairSO(positiveSequenceSO _own, negativeSequenceSO _for);
	pairSO(){}
};

